const secrets		= require("./bot_secrets.json");
const config		= require("./config.json");

const Discord		= require('discord.js');
const logger		= require("./logger.js");
const mysql         = require('mysql2/promise');

const db_table      = "mod_featured"

const mysq_data = {
    host:       secrets.mysql.host,
    port:       secrets.mysql.port,  
    user:       secrets.mysql.user + "1",
    password:   secrets.mysql.pass,
    database:   secrets.mysql.db
}

function log_info(module, submodule, func, msg){
	message = `${msg}`.replace(/(\r\n|\n|\r)/gm, " ")
    console.log(`{"module":"${module}","submodule":"${submodule}","function":"${func}","message":"${message}"}`)
    logger.info(`{"module":"${module}","submodule":"${submodule}","function":"${func}","message":"${message}"}`)
}

function log_warn(module, submodule, func, msg){
	message = `${msg}`.replace(/(\r\n|\n|\r)/gm, " ")
    console.log(`{"module":"${module}","submodule":"${submodule}","function":"${func}","message":"${message}"}`)
    logger.warn(`{"module":"${module}","submodule":"${submodule}","function":"${func}","message":"${message}"}`)
}

function log_error(module, submodule, func, msg){
	message = `${msg}`.replace(/(\r\n|\n|\r)/gm, " ")
    console.log (`{"module":"${module}","submodule":"${submodule}","function":"${func}","message":"${message}"}`)
    logger.error(`{"module":"${module}","submodule":"${submodule}","function":"${func}","message":"${message}"}`)
}

// Establish Mysql Connection


// Establish Discord Connection
const client  = new Discord.Client({ intents: [Discord.GatewayIntentBits.Guilds, Discord.GatewayIntentBits.GuildMessages, Discord.GatewayIntentBits.MessageContent, Discord.GatewayIntentBits.GuildMessageReactions, ] })
client.login(secrets.discord)

client.on('ready', bot => {
	log_info('PL-Reactions', 'client.on', 'ready', 'Online')
});

client.on('warn', warn => {
	log_warn('PL-Reactions', 'client.on', 'warn', warn.stack)
});

client.on('error', err => {
	log_error('PL-Reactions', 'client.on', 'error', err.stack)
});


client.on('messageCreate', async message => {
    try {
        if(message.author.bot == true){return}

        // Get Chhannels for Reactions Module in tis guild
        let channels = Object.keys(config[message.guildId].module.reactions)

        // Check if channel is in config
        if(channels.includes(message.channel.id))
        {
            // Act on amount of attachements
            if(message.attachments.size === 1)
            {
                // Add reactions
                for (let item of config[message.guildId].module.reactions[message.channel.id].emojis) {
                    await message.react(item)
                }
            }
            else if (message.attachments.size < 1)
            {
                if(!config[message.guildId].module.reactions[message.channel.id].messages){
                    await message.reply("Sending text in this chat is prohibited").then(msg => {setTimeout(() => msg.delete(), config[message.guildId].module.reactions[message.channel.id].timeout)});
                    message.delete();
                }
            }
            else if (message.attachments.size > 1)
            {
                if(!config[message.guildId].module.reactions[message.channel.id].multi_images){
                    await message.reply("Sending multible attachments in this chat is prohibited").then(msg => {setTimeout(() => msg.delete(), config[message.guildId].module.reactions[message.channel.id].timeout)});	
                    message.delete();
                }			
            }
            return;
        }

    } catch (error){
        log_error('PL-Reactions', '', 'add reactions', error.stack)
    }	
});

client.on('messageReactionAdd', async (reaction, user) => {
	if(user.bot == true){return}

    try {

        // Early return if not in config
        let channels = Object.keys(config[reaction.message.guildId].module.reactions)
        
        if(!channels.includes(reaction.message.channel.id)){return}

        if(reaction.message.author.id == user.id && !config[reaction.message.guildId].module.reactions[reaction.message.channel.id].self_react)        
        {   // Remove Self Reacts
            reaction.users.remove(user.id);
            return;
        }
        
        if(reaction.emoji.id === config[reaction.message.guildId].module.reactions[reaction.message.channel.id].feedback_emoji) 
        {   // Copy on React

            let reactor = await reaction.message.guild.members.cache.get(user.id)
            if(reactor.roles.cache.has(config[reaction.message.guildId].common.roles.feedback) == false && reactor.roles.cache.has(config[reaction.message.guildId].common.roles.active) == false && reactor.roles.cache.has(config[reaction.message.guildId].common.roles.shareholder) == false){ return }
            // Permission Check
            if(reactor.roles.cache.has("1330826960804384788") == false) {return} 


            let feedback_channel = await client.channels.cache.get(config[reaction.message.guildId].module.reactions[reaction.message.channel.id].feedback_channel);

            let authordata = {
                name: `Image by: ${reaction.message.author.username}`, 
                iconURL: reaction.message.author.displayAvatarURL()
            }
            
            if(!reaction.message.attachments.first()){return}
            //Image Embed
            let responseEmbed = new Discord.EmbedBuilder()
                .setColor(config[reaction.message.guildId].common.embed)
                .setImage(reaction.message.attachments.first().url)
                .setAuthor(authordata)
                .setDescription(`In channel <#${reaction.message.channel.id}>\n Jump to [original](${reaction.message.url})`)
            

            feedback_channel.send({embeds: [responseEmbed], content: `<@${user.id}> would like to give feedback on your photo <@${reaction.message.author.id}>.` }, )
            
            //Transient Embed
            let transientEmbed = new Discord.EmbedBuilder()
                .setColor(config[reaction.message.guildId].common.embed)
                .setDescription(`The picture got copied to <#${config[reaction.message.guildId].module.reactions[reaction.message.channel.id].feedback_channel}>. Tell the creator what you think about it.`);
            
            reaction.message.channel.send({ embeds: [transientEmbed] }).then(msg => {setTimeout(() => msg.delete(), config[reaction.message.guildId].module.reactions[reaction.message.channel.id].timeout)});
            
        }

        // add to database at upvote thresholfd
        if(reaction.emoji.id == config[reaction.message.guildId].module.reactions[reaction.message.channel.id].upvote_emoji)
        {
            // Copy on X Upvotes
            if(reaction.message.reactions.cache.get(config[reaction.message.guildId].module.reactions[reaction.message.channel.id].upvote_emoji).count > config[reaction.message.guildId].module.reactions[reaction.message.channel.id].upvotes)
            {
                
                var mysql_conn = await  mysql.createConnection(mysq_data);


                //Fetch Database Entry
                const [results, fields] = await mysql_conn.execute('SELECT COUNT(*) FROM ' + db_table + ' WHERE guild = ? AND channel = ? AND message = ? ', [reaction.message.guildId, reaction.message.channelId, reaction.message.id])

                if(results[0]['COUNT(*)'] < 1)
                {   
                    const [results_insert, fields_insert] = await mysql_conn.execute('INSERT INTO ' + db_table + ' (guild, channel, message) VALUES (?,?,?)', [reaction.message.guildId, reaction.message.channelId, reaction.message.id])

                    let upvote_channel = await client.channels.cache.get(config[reaction.message.guildId].module.reactions[reaction.message.channel.id].upvote_channel);

                    //Image Embed
                    let authordata = {
                        name: `Image by: ${reaction.message.author.username}`, 
                        iconURL: reaction.message.author.displayAvatarURL()
                    }

                    let upvoteEmbed = new Discord.EmbedBuilder()
                        .setColor(config[reaction.message.guildId].common.embed)
                        .setImage(reaction.message.attachments.first().url)
                        .setAuthor(authordata)
                        .setDescription(`In channel <#${reaction.message.channel.id}> \n Jump to [original](${reaction.message.url})`)
                    
                    upvote_channel.send({ embeds: [upvoteEmbed] })



                }
                mysql_conn.end()
            }
        }

    } catch (error){
        log_error('PL-Reactions', '', 'copy on reaction', error.stack)
    }
});

client.on('interactionCreate', async interaction => {
	if (!interaction.isCommand()){return}

    try {
        if(interaction.commandName == 'featurepost')
        {
            let description = interaction.options.getString('description')
            let link        = interaction.options.getString('link').split("/")
            let channel     = interaction.guild.channels.cache.get(link[5]);
            let msgid       = link[6]

            // Permission Check
            if(interaction.member.roles.cache.has(config[interaction.guildId].common.roles.specialist) == false && interaction.member.roles.cache.has(config[interaction.guildId].common.roles.admin) == false){
                interaction.reply({ content:"You do not have the ppermissions to use this command", ephemeral: true }).catch(error => {
                    log_error('PL-Reactions', '', 'interaction reply', error.stack)
                })                
                return
            }

            channel.messages.fetch(msgid)
            .then(async message => {
                //Image Embed
                let featured_channel = await client.channels.cache.get(config[interaction.guildId].module.reactions.general.featured_channel);
                let authordata = {
                    name: `Image by: ${message.author.username}`, 
                    iconURL: message.author.displayAvatarURL()
                }

                let upvoteEmbed = new Discord.EmbedBuilder()
                    .setColor(config[interaction.guildId].common.embed)
                    .setImage(message.attachments.first().url)
                    .setAuthor(authordata)
                    .setDescription(`In channel <#${message.channel.id}>\n Jump to [original](${message.url})`)
                    
                
                if(description != null){
                    upvoteEmbed.addFields(
                        {name:'Specialist Member:'            , value: `<@${interaction.user.id}>`},
                        {name:'Specialist Member Description:', value: `${description}`}
                    )
                }
                else {
                    upvoteEmbed.addFields(
                        {name:'Specialist Member:'            , value: `<@${interaction.user.id}>`},
                    )
                }

                featured_channel.send({ embeds: [upvoteEmbed] })
                interaction.reply({ content:"Post got featured", ephemeral: true }).catch(error => {
                    log_error('PL-Reactions', '', 'interaction reply', error.stack)
                })

                if(!message.member.roles.cache.has(config[interaction.guildId].common.roles.featured)){
                    message.member.roles      .add(config[interaction.guildId].common.roles.featured)
                }
                

            })
            .catch( error => {
                interaction.reply({ content:"Error message not found", ephemeral: true }).catch(error => {
                    log_error('PL-Reactions', '', 'interaction reply', error.stack)
                })
                log_error('PL-Reactions', '', 'feature post', error.stack)
            })
        }
    } catch (error){
        log_error('PL-Reactions', '', 'feature post', error.stack)
    }
});